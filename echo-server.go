package main

import (
	"io"
	"log"
	"net"
)

// echo will echo whatever is thrown at it
func echo(conn net.Conn) {
	defer conn.Close()

	if _, err := io.Copy(conn, conn); err != nil {
		log.Fatalln("Unable to read/write data")
	}
}

func main() {
	listener, err := net.Listen("tcp", ":15282")
	if err != nil {
		log.Fatalln("Cant listen to port 15282")
	}
	log.Println("Listening on 0.0.0.0:15282")

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalln("Something went wrong")
		}
		log.Println("Received connection")

		go echo(conn)
	}

}
